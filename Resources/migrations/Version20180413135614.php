<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Create a table for Single task.
 */
class Version20180413135614 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA chill_task');
        $this->addSql('CREATE SEQUENCE chill_task.single_task_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE chill_task.single_task (id INT NOT NULL, assignee_id INT DEFAULT NULL, person_id INT DEFAULT NULL, circle_id INT DEFAULT NULL, type VARCHAR(255) NOT NULL, current_states JSONB NOT NULL, title TEXT NOT NULL, description TEXT NOT NULL, start_date DATE DEFAULT NULL, end_date DATE DEFAULT NULL, warning_interval INTERVAL DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_194CB3D859EC7D60 ON chill_task.single_task (assignee_id)');
        $this->addSql('CREATE INDEX IDX_194CB3D8217BBB47 ON chill_task.single_task (person_id)');
        $this->addSql('CREATE INDEX IDX_194CB3D870EE2FF6 ON chill_task.single_task (circle_id)');
        $this->addSql('COMMENT ON COLUMN chill_task.single_task.warning_interval IS \'(DC2Type:dateinterval)\'');
        $this->addSql('ALTER TABLE chill_task.single_task ADD CONSTRAINT FK_194CB3D859EC7D60 FOREIGN KEY (assignee_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_task.single_task ADD CONSTRAINT FK_194CB3D8217BBB47 FOREIGN KEY (person_id) REFERENCES chill_person_person (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_task.single_task ADD CONSTRAINT FK_194CB3D870EE2FF6 FOREIGN KEY (circle_id) REFERENCES scopes (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SCHEMA chill_task CASCADE');
        
    }
}
