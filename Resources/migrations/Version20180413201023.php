<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Create recurring task
 */
class Version20180413201023 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE chill_task.recurring_task_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE chill_task.recurring_task (id INT NOT NULL, assignee_id INT DEFAULT NULL, person_id INT DEFAULT NULL, circle_id INT DEFAULT NULL, type VARCHAR(255) NOT NULL, current_states JSONB NOT NULL, title TEXT NOT NULL, description TEXT NOT NULL, first_occurence_end_date DATE NOT NULL, last_occurence_end_date DATE NOT NULL, occurence_frequency VARCHAR(255) NOT NULL, occurence_start_date INTERVAL NOT NULL, occurence_warning_interval INTERVAL DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_9F663B9059EC7D60 ON chill_task.recurring_task (assignee_id)');
        $this->addSql('CREATE INDEX IDX_9F663B90217BBB47 ON chill_task.recurring_task (person_id)');
        $this->addSql('CREATE INDEX IDX_9F663B9070EE2FF6 ON chill_task.recurring_task (circle_id)');
        $this->addSql('COMMENT ON COLUMN chill_task.recurring_task.occurence_start_date IS \'(DC2Type:dateinterval)\'');
        $this->addSql('COMMENT ON COLUMN chill_task.recurring_task.occurence_warning_interval IS \'(DC2Type:dateinterval)\'');
        $this->addSql('ALTER TABLE chill_task.recurring_task ADD CONSTRAINT FK_9F663B9059EC7D60 FOREIGN KEY (assignee_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_task.recurring_task ADD CONSTRAINT FK_9F663B90217BBB47 FOREIGN KEY (person_id) REFERENCES chill_person_person (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_task.recurring_task ADD CONSTRAINT FK_9F663B9070EE2FF6 FOREIGN KEY (circle_id) REFERENCES scopes (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_task.single_task ADD recurringTask_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_task.single_task ADD CONSTRAINT FK_194CB3D840868C31 FOREIGN KEY (recurringTask_id) REFERENCES chill_task.recurring_task (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_194CB3D840868C31 ON chill_task.single_task (recurringTask_id)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE chill_task.single_task DROP CONSTRAINT FK_194CB3D840868C31');
        $this->addSql('DROP SEQUENCE chill_task.recurring_task_id_seq CASCADE');
        
        $this->addSql('DROP TABLE chill_task.recurring_task');
        $this->addSql('ALTER TABLE chill_task.single_task DROP recurringTask_id');
    }
}
