<?php
/*
 * Copyright (C) 2018 Julien Fastré <julien.fastre@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\TaskBundle\Menu;

use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Knp\Menu\MenuItem;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Chill\TaskBundle\Security\Authorization\TaskVoter;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class PersonMenuBuilder implements LocalMenuBuilderInterface
{
    /**
     *
     * @var TranslatorInterface
     */
    protected $translator;
    
    /**
     *
     * @var AuthorizationCheckerInterface
     */
    protected $authorizationChecker;
    
    public function __construct(
        AuthorizationCheckerInterface $authorizationChecker,
        TranslatorInterface $translator)
    {
        $this->translator = $translator;
        $this->authorizationChecker = $authorizationChecker;
    }

    
    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        /* @var $person \Chill\PersonBundle\Entity\Person */
        $person = $parameters['person'] ?? null;
        
        if ($this->authorizationChecker->isGranted(TaskVoter::SHOW, $person)) {
            $menu->addChild(
                $this->translator->trans('Tasks'), [
                    'route' => 'chill_task_singletask_list',
                    'routeParameters' => $menuId === 'person' ? 
                        [ 'person_id' => $person->getId() ]
                        :
                        null,
                ])
                ->setExtra('order', 400)
                ;
            if ($menuId === 'section') {
                $menu->setExtra('icons', 'tasks');
            }
        }
    }

    public static function getMenuIds(): array
    {
        return ['person'];
    }
}
