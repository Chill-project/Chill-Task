<?php
/*
 * Copyright (C) 2018 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\TaskBundle\Menu;

use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Knp\Menu\MenuItem;
use Chill\TaskBundle\Templating\UI\CountNotificationTask;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Chill\TaskBundle\Repository\SingleTaskRepository;
use Symfony\Component\Translation\TranslatorInterface;
use Chill\MainBundle\Entity\User;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Chill\TaskBundle\Security\Authorization\TaskVoter;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class UserMenuBuilder implements LocalMenuBuilderInterface
{
    
    /**
     *
     * @var CountNotificationTask
     */
    public $counter;
    
    /*
     * @var TokenStorageInterface
     */
    public $tokenStorage;
    
    /**
     *
     * @var TranslatorInterface
     */
    public $translator;
    
    /**
     *
     * @var AuthorizationCheckerInterface
     */
    public $authorizationChecker;
    
    public function __construct(
        CountNotificationTask $counter,
        TokenStorageInterface $tokenStorage,
        TranslatorInterface $translator,
        AuthorizationCheckerInterface $authorizationChecker
    ) {
        $this->counter = $counter;
        $this->tokenStorage = $tokenStorage;
        $this->translator = $translator;
        $this->authorizationChecker = $authorizationChecker;
    }
    
    public static function getMenuIds(): array
    {
        return [ 'user' ];
    }
    
    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        if (FALSE === $this->authorizationChecker->isGranted(TaskVoter::SHOW)) {
            return;
        }
        
        $user = $this->tokenStorage->getToken()->getUser();
        $ended = $this->counter->countNotificationEnded($user);
        $warning = $this->counter->countNotificationWarning($user);
        
        if ($ended > 0) {
            $this->addItemInMenu(
                $menu, 
                $user, 
                '%number% tasks over deadline', 
                'My tasks over deadline',
                SingleTaskRepository::DATE_STATUS_ENDED,
                $ended, 
                -15);
        }
        
        if ($warning > 0) {
            $this->addItemInMenu(
                $menu, 
                $user, 
                '%number% tasks near deadline', 
                'My tasks near deadline', 
                SingleTaskRepository::DATE_STATUS_WARNING,
                $warning, 
                -14);
        }
        
        $menu->addChild("My tasks", [
                'route' => 'chill_task_single_my_tasks'
            ])
            ->setExtras([
                'order' => -10,
                'icon' => 'tasks'
            ]);
    }
    
    protected function addItemInMenu(MenuItem $menu, User $u, $message, $title, $status, $number, $order)
    {
        if ($number > 0) {
            $menu->addChild(
                $this->translator->transChoice($message, $number), 
                [
                    'route' => 'chill_task_singletask_list',
                    'routeParameters' => [
                        'user_id' => $u->getId(),
                        'status' => [
                            $status
                            ],
                        'hide_form' => true,
                        'title' => $this->translator->trans($title)
                ]
            ])
                ->setExtras([
                    'order'=> $order,
                    'icon' => 'exclamation-triangle',
                    'entryclass' => 'user_menu__entry--warning-entry'
                ]);
        }
    }
}
