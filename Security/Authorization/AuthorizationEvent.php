<?php
/*
 * 
 */
namespace Chill\TaskBundle\Security\Authorization;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class AuthorizationEvent extends Event
{
    /**
     * @var Chill\TaskBundle\Entity\AbstractTask|\Chill\PersonBundle\Entity\Person|null
     */
    protected $subject;
    
    /**
     *
     * @var string
     */
    protected $attribute;
    
    /**
     *
     * @var TokenInterface
     */
    protected $token;
    
    /**
     *
     * @var bool
     */
    protected $vote;
    
    const VOTE = 'chill_task.vote';
    
    public function __construct(
        $subject, 
        $attribute, 
        TokenInterface $token
    ) {
        $this->subject = $subject;
        $this->attribute = $attribute;
        $this->token = $token;
    }

    public function getSubject()
    {
        return $this->subject;
    }

    public function getAttribute()
    {
        return $this->attribute;
    }

    public function getToken(): TokenInterface
    {
        return $this->token;
    }
    
    public function getVote()
    {
        return $this->vote;
    }
   
    public function setVote($vote)
    {
        $this->vote = $vote;
        
        return $this;
    }

    public function hasVote()
    {
        return $this->vote !== NULL;
    }
    
    public function removeVote()
    {
        $this->vote = NULL;
    }
}
