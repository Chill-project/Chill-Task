<?php
/*
 * Copyright (C) 2018 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\TaskBundle\Security\Authorization;

use Chill\MainBundle\Security\Authorization\AbstractChillVoter;
use Chill\TaskBundle\Entity\AbstractTask;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Chill\MainBundle\Security\Authorization\AuthorizationHelper;
use Chill\PersonBundle\Security\Authorization\PersonVoter;
use Psr\Log\LoggerInterface;
use Chill\MainBundle\Security\ProvideRoleHierarchyInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Chill\MainBundle\Entity\User;
use Chill\PersonBundle\Entity\Person;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Chill\TaskBundle\Security\Authorization\AuthorizationEvent;

/**
 *
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class TaskVoter extends AbstractChillVoter implements ProvideRoleHierarchyInterface
{
    const CREATE = 'CHILL_TASK_TASK_CREATE';
    const UPDATE = 'CHILL_TASK_TASK_UPDATE';
    const SHOW   = 'CHILL_TASK_TASK_SHOW';
    const DELETE   = 'CHILL_TASK_TASK_DELETE';

    const ROLES = [
        self::CREATE,
        self::UPDATE,
        self::SHOW,
        self::DELETE
    ];

    /**
     *
     * @var AuthorizationHelper
     */
    protected $authorizationHelper;

    /**
     *
     * @var AccessDecisionManagerInterface
     */
    protected $accessDecisionManager;

    /**
     *
     * @var LoggerInterface
     */
    protected $logger;
    
    /**
     *
     * @var EventDispatcherInterface
     */
    protected $eventDispatcher;

    public function __construct(
        AccessDecisionManagerInterface $accessDecisionManager,
        AuthorizationHelper $authorizationHelper,
        EventDispatcherInterface $eventDispatcher,
        LoggerInterface $logger
    ) {
        $this->accessDecisionManager = $accessDecisionManager;
        $this->authorizationHelper = $authorizationHelper;
        $this->eventDispatcher = $eventDispatcher;
        $this->logger = $logger;
    }

    public function supports($attribute, $subject)
    {
        return ($subject instanceof AbstractTask && in_array($attribute, self::ROLES))
            ||
            ($subject instanceof Person && \in_array($attribute, [ self::CREATE, self::SHOW ]))
            ||
            (NULL === $subject && $attribute === self::SHOW )
            ;
    }

    /**
     *
     * @param string $attribute
     * @param AbstractTask $subject
     * @param TokenInterface $token
     * @return boolean
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $this->logger->debug(sprintf("Voting from %s class", self::class));

        if (!$token->getUser() instanceof User) {
            return false;
        }
        
        $event = new AuthorizationEvent($subject, $attribute, $token);
        
        $this->eventDispatcher->dispatch(AuthorizationEvent::VOTE, $event);
        
        if ($event->hasVote()) {
            
            $this->logger->debug("The TaskVoter is overriding by "
                .AuthorizationEvent::VOTE, [
                    'vote' => $event->getVote(),
                    'task_id' => $subject->getId()
                ]);
            
            return $event->getVote();
        }

        if ($subject instanceof AbstractTask) {
            if ($subject->getPerson() === null) {
                throw new \LogicException("You should associate a person with task "
                    . "in order to check autorizations");
            }
            
            $person = $subject->getPerson();
        } elseif ($subject instanceof Person) {
            $person = $subject;
        } else {
            // subject is null. We check  that at least one center is reachable
            $centers = $this->authorizationHelper->getReachableCenters($token->getUser(), new Role($attribute));
            
            return count($centers) > 0;
        }

        if (!$this->accessDecisionManager->decide($token, [PersonVoter::SEE], $person)) {

            return false;
        }

        return $this->authorizationHelper->userHasAccess(
            $token->getUser(),
            $subject,
            $attribute
            );
    }

    public function getRoles()
    {
        return self::ROLES;
    }

    public function getRolesWithHierarchy(): array
    {
        return [
            'Task' => self::ROLES
        ];
    }

    public function getRolesWithoutScope()
    {
        return [];
    }

}
