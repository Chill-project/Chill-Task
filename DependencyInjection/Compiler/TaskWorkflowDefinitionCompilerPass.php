<?php
/*
 * Copyright (C) 2018 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\TaskBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Chill\TaskBundle\Workflow\TaskWorkflowManager;
use Symfony\Component\DependencyInjection\Reference;
use Chill\TaskBundle\Templating\UI\CountNotificationTask;
use Chill\TaskBundle\Event\Lifecycle\TaskLifecycleEvent;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class TaskWorkflowDefinitionCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition(TaskWorkflowManager::class)) {
            throw new \LogicException("The service ".TaskWorkflowManager::class." is "
                . "not registered");
        }
        
        $workflowManagerDefinition = $container->getDefinition(TaskWorkflowManager::class);
        $counterDefinition = $container->getDefinition(CountNotificationTask::class);
        $lifecycleDefinition = $container->getDefinition(TaskLifecycleEvent::class);
        
        foreach ($container->findTaggedServiceIds('chill_task.workflow_definition') as $id => $tags) {
            // registering the definition to manager
            $workflowManagerDefinition
                ->addMethodCall('addDefinition', [new Reference($id)]);
            // adding a listener for currentStatus changes
            $definition = $container->getDefinition($id);
            $workflowManagerDefinition
                ->addTag('kernel.event_listener', [
                    'event' => sprintf('workflow.%s.entered', $definition->getClass()::getAssociatedWorkflowName()), 
                    'method' => 'onTaskStateEntered', 
                    'priority' => -255 
                    ]);
            $counterDefinition
                ->addTag('kernel.event_listener', [
                    'event' => sprintf('workflow.%s.entered', $definition->getClass()::getAssociatedWorkflowName()),
                    'method' => 'resetCacheOnNewStates',
                    'priority' => 0
                ]);
            $lifecycleDefinition
                ->addTag('kernel.event_listener', [
                    'event' => sprintf('workflow.%s.transition', $definition->getClass()::getAssociatedWorkflowName()),
                    'method' => 'onTransition',
                    'priority' => 0
                ]);
        }
    }
}
