<?php

namespace Chill\TaskBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Chill\TaskBundle\Security\Authorization\TaskVoter;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Chill\TaskBundle\Workflow\TaskWorkflowManager;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class ChillTaskExtension extends Extension implements PrependExtensionInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services/controller.yml');
        $loader->load('services/security.yml');
        $loader->load('services/repositories.yml');
        $loader->load('services/workflow.yml');
        $loader->load('services/templating.yml');
        $loader->load('services/menu.yml');
        $loader->load('services/event.yml');
        $loader->load('services/timeline.yml');
        $loader->load('services/fixtures.yml');
        $loader->load('services/form.yml');
    }

    public function prepend(ContainerBuilder $container)
    {
        $this->prependAuthorization($container);
        $this->prependRoute($container);
        $this->prependWorkflows($container);
    }
    
    protected function prependRoute(ContainerBuilder $container)
    {
        //declare routes for task bundle
        $container->prependExtensionConfig('chill_main', array(
           'routing' => array(
              'resources' => array(
                 '@ChillTaskBundle/Resources/config/routing.yml'
              )
           )
        ));
    }
    
    protected function prependAuthorization(ContainerBuilder $container)
    {
        $container->prependExtensionConfig('security', array(
           'role_hierarchy' => array(
               TaskVoter::UPDATE => [TaskVoter::SHOW],
               TaskVoter::CREATE => [TaskVoter::SHOW]
           )
        ));
    }
    
    protected function prependWorkflows(ContainerBuilder $container)
    {
        $container->prependExtensionConfig('framework', [
            'workflows' => [
                'task_default' => [
                    'marking_store' => [
                        'type' => 'multiple_state',
                        'arguments' => [
                            'currentStates'
                        ],
                    ],
                    'type'          => 'state_machine',
                    'support_strategy' => TaskWorkflowManager::class,
                    'places' => [ 'new', 'in_progress', 'closed', 'canceled'],
                    'initial_place' => 'new',
                    'transitions' => [
                        'start' => [
                            'from' => 'new',
                            'to'   => 'in_progress'
                        ],
                        'close' => [
                            'from' => ['new', 'in_progress'],
                            'to'   => 'closed'
                        ],
                        'cancel' => [
                            'from' => ['new', 'in_progress'],
                            'to'   => 'canceled'
                        ]
                    ]
                ]
            ]
        ]);
    }
}
