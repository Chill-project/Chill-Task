<?php
/*
 * Copyright (C) 2018 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\TaskBundle\Templating\UI;

use Chill\MainBundle\Templating\UI\NotificationCounterInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Chill\MainBundle\Entity\User;
use Chill\TaskBundle\Repository\SingleTaskRepository;
use Psr\Cache\CacheItemPoolInterface;
use Symfony\Component\Workflow\Event\Event;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class CountNotificationTask implements NotificationCounterInterface
{
    /**
     *
     * @var SingleTaskRepository
     */
    protected $singleTaskRepository;
    
    /**
     *
     * @var CacheItempPoolInterface
     */
    protected $cachePool;
    
    const CACHE_KEY = 'chill_task.count_notifications.user.%d.%s';
    
    public function __construct(
        SingleTaskRepository $singleTaskRepository,
        CacheItemPoolInterface $cachePool
    ) {
        $this->singleTaskRepository = $singleTaskRepository;
        $this->cachePool = $cachePool;
    }
    
    public function countNotification(UserInterface $u): int
    {
        return 
            $this->countNotificationEnded($u) 
                + $this->countNotificationWarning($u);
    }
    
    public function countNotificationEnded(UserInterface $u): int
    {
        return $this->_countNotification($u, SingleTaskRepository::DATE_STATUS_ENDED);
    }
    
    public function countNotificationWarning(UserInterface $u): int
    {
        return $this->_countNotification($u, SingleTaskRepository::DATE_STATUS_WARNING);
    }
    
    protected function _countNotification(UserInterface $u, $status)
    {
        if (!$u instanceof User) {
            return 0;
        }
        
        $sumCache  = $this->cachePool->getItem($this->getCacheKey($u, $status));
        
        if ($sumCache->isHit()) {
            return $sumCache->get();
        }
        
        $params = [
            'user' => $u,
            'is_closed' => false
        ];
        
        $sum = $this->singleTaskRepository->countByParameters(
                \array_merge($params, [ 'date_status' => $status ])
            );
        
        $sumCache->set($sum);
        $this->cachePool->save($sumCache);
        
        return $sum;
    }
    
    public function addNotification(UserInterface $u): int
    {
        return $this->countNotification($u);
    }
    
    public function resetCacheOnNewStates(Event $e)
    {
        /* @var $task \Chill\TaskBundle\Entity\SingleTask */
        $task = $e->getSubject();
        
        if (NULL !== $task->getAssignee()) {
            foreach ([
                SingleTaskRepository::DATE_STATUS_ENDED,
                SingleTaskRepository::DATE_STATUS_WARNING
            ] as $status) {
                $key = $this->getCacheKey($task->getAssignee(), $status);
                $sumCache = $this->cachePool->getItem($key);

                if ($sumCache->isHit()) {
                    $this->cachePool->deleteItem($key);
                }
            }
        }
    }
    
    private function getCacheKey(User $u, $status)
    {
        return sprintf(self::CACHE_KEY,  $u->getId(), $status);
    }
}
