<?php
/*
 * Copyright (C) 2018 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\TaskBundle\Templating;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Chill\TaskBundle\Entity\AbstractTask;
use Chill\TaskBundle\Workflow\TaskWorkflowManager;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class TaskTwigExtension extends \Twig_Extension
{
    /**
     *
     * @var TaskWorkflowManager
     */
    protected $taskWorkflowManager;
    
    public function __construct(TaskWorkflowManager $taskWorkflowManager)
    {
        $this->taskWorkflowManager = $taskWorkflowManager;
    }

    
    public function getFunctions()
    {
        return [
            new TwigFunction('task_workflow_metadata', [ $this, 'getWorkflowMetadata' ] )
        ];
    }
    
    public function getWorkflowMetadata(
        AbstractTask $task, 
        string $key, 
        $metadataSubject = null, 
        string $name = null
    ) {
        return $this->taskWorkflowManager->getWorkflowMetadata($task, $key, $metadataSubject, $name);
    }
}
