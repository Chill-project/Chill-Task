<?php

namespace Chill\TaskBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\Collection;

/**
 * SingleTask
 *
 * @ORM\Table(
 *   "chill_task.single_task",
 *   indexes={
 *     @ORM\Index(
 *        name="by_type",
 *        columns={ "type" }
 *     ),
 *     @ORM\Index(
 *        name="by_current_state",
 *        columns={ "current_states" }
 *     )
 *   }
 * )
 * @ORM\Entity(repositoryClass="Chill\TaskBundle\Repository\SingleTaskRepository")
 */
class SingleTask extends AbstractTask
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="date", nullable=true)
     * @Assert\Date()
     * 
     * @Assert\Expression(
     *  "value === null or this.getEndDate() === null or value < this.getEndDate()",
     *  message="The start date must be before the end date"
     * )
     * 
     * @Assert\Expression(
     *  "value === null or this.getWarningDate() === null or this.getWarningDate() > this.getStartDate()",
     *  message="The start date must be before warning date"
     * )
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="date", nullable=true)
     * @Assert\Date()
     */
    private $endDate;

    /**
     * @var \DateInterval
     *  and this.getEndDate() === null
     *
     * @ORM\Column(name="warning_interval", type="dateinterval", nullable=true)
     * 
     * @Assert\Expression(
     *  "!(value != null and this.getEndDate() == null)",
     *  message="An end date is required if a warning interval is set"
     * )
     * 
     * 
     */
    private $warningInterval;
    
    /**
     *
     * @var RecurringTask
     * @ORM\ManyToOne(
     *  targetEntity="RecurringTask",
     *  inversedBy="singleTasks"
     * )
     */
    private $recurringTask;
    
    /**
     *
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\OneToMany(
     *  targetEntity="\Chill\TaskBundle\Entity\Task\SingleTaskPlaceEvent",
     *  mappedBy="task",
     *  cascade={ "remove" }
     * )
     */
    private $taskPlaceEvents;
    
    public function __construct()
    {
        $this->taskPlaceEvents = $events = new \Doctrine\Common\Collections\ArrayCollection;
        
        parent::__construct();
    }

    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return SingleTask
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return SingleTask
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set warningInterval
     *
     * @param string $warningInterval
     *
     * @return SingleTask
     */
    public function setWarningInterval($warningInterval)
    {
        $this->warningInterval = $warningInterval;

        return $this;
    }

    /**
     * Get warningInterval
     *
     * @return \DateInterval
     */
    public function getWarningInterval()
    {
        return $this->warningInterval;
    }
    
    /**
     * Get the Warning date, computed from the difference between the 
     * end date and the warning interval
     * 
     * Return null if warningDate or endDate is null
     * 
     * @return \DateTimeImmutable
     */
    public function getWarningDate()
    {
        if ($this->getWarningInterval() === null) {
            return null;
        }
        
        if ($this->getEndDate() === null) {
            return null;
        }
        
        return \DateTimeImmutable::createFromMutable($this->getEndDate())
            ->sub($this->getWarningInterval());
    }
    
    function getRecurringTask(): RecurringTask
    {
        return $this->recurringTask;
    }

    function setRecurringTask(RecurringTask $recurringTask)
    {
        $this->recurringTask = $recurringTask;
    }

    public function getTaskPlaceEvents(): Collection
    {
        return $this->taskPlaceEvents;
    }

    public function setTaskPlaceEvents(Collection $taskPlaceEvents)
    {
        $this->taskPlaceEvents = $taskPlaceEvents;
        
        return $this;
    }
}

