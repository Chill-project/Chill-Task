<?php

namespace Chill\TaskBundle\Entity\Task;

use Doctrine\ORM\Mapping as ORM;
use Chill\MainBundle\Entity\User;

/**
 * AbstractTaskPlaceEvent
 *
 * @ORM\MappedSuperclass()
 */
class AbstractTaskPlaceEvent
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime_immutable
     *
     * @ORM\Column(name="datetime", type="datetime_immutable")
     */
    private $datetime;

    /**
     * @var string
     *
     * @ORM\Column(name="transition", type="string", length=255)
     */
    private $transition = '';

    /**
     * @var string
     *
     * @ORM\Column(name="data", type="json")
     */
    private $data = [];
    
    /**
     * 
     * @var User
     * @ORM\ManyToOne(
     *  targetEntity="\Chill\MainBundle\Entity\User"
     * )
     */
    private $author;

    public function __construct()
    {
        $this->datetime = new \DateTimeImmutable('now');
    }

        /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datetime.
     *
     * @param datetime_immutable $datetime
     *
     * @return AbstractTaskPlaceEvent
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Get datetime.
     *
     * @return datetime_immutable
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * Set transition.
     *
     * @param string $transition
     *
     * @return AbstractTaskPlaceEvent
     */
    public function setTransition($transition)
    {
        $this->transition = $transition;

        return $this;
    }

    /**
     * Get transition.
     *
     * @return string
     */
    public function getTransition()
    {
        return $this->transition;
    }

    /**
     * Set data.
     *
     * @param string $data
     *
     * @return AbstractTaskPlaceEvent
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data.
     *
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }
    
    public function getAuthor(): User
    {
        return $this->author;
    }

    public function setAuthor(User $author)
    {
        $this->author = $author;
        
        return $this;
    }


}
