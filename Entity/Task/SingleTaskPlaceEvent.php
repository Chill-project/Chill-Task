<?php
/*
 * Copyright (C) 2018 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\TaskBundle\Entity\Task;

use Doctrine\ORM\Mapping as ORM;
use Chill\TaskBundle\Entity\SingleTask;

/**
 * 
 *
 * @ORM\Table(
 *  "chill_task.single_task_place_event",
 *  indexes={
 *    @ORM\Index(
 *      name="transition_task_date", 
 *      columns={"task_id", "transition", "datetime"}
 *    ),
 *    @ORM\Index(
 *      name="transition_task",
 *      columns={"task_id", "transition"}
 *    )
 *  })
 * @ORM\Entity()
 * 
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class SingleTaskPlaceEvent extends AbstractTaskPlaceEvent
{
    /**
     *
     * @var SingleTask
     * @ORM\ManyToOne(
     *  targetEntity="\Chill\TaskBundle\Entity\SingleTask",
     *  inversedBy="taskPlaceEvents"
     * )
     */
    protected $task;
    
    public function getTask(): SingleTask
    {
        return $this->task;
    }

    public function setTask(SingleTask $task)
    {
        $this->task = $task;
        
        return $this;
    }


}
