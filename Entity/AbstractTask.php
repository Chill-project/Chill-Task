<?php

namespace Chill\TaskBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Chill\MainBundle\Entity\User;
use Chill\PersonBundle\Entity\Person;
use Chill\MainBundle\Entity\Scope;
use Chill\MainBundle\Entity\HasScopeInterface;
use Chill\MainBundle\Entity\HasCenterInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Chill\MainBundle\Validator\Constraints\Entity\UserCircleConsistency;

/**
 * AbstractTask
 *
 * @ORM\MappedSuperclass()
 * @UserCircleConsistency(
 *  "CHILL_TASK_TASK_SHOW",
 *  getUserFunction="getAssignee"
 * )
 */
abstract class AbstractTask implements HasScopeInterface, HasCenterInterface
{

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var json
     *
     * @ORM\Column(name="current_states", type="json")
     */
    private $currentStates = [];

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="text")
     * @Assert\NotBlank()
     */
    private $title = '';

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description = '';
    
    /**
     *
     * @var User
     * @ORM\ManyToOne(
     *  targetEntity="\Chill\MainBundle\Entity\User"
     * )
     */
    private $assignee;
    
    /**
     *
     * @var Person
     * @ORM\ManyToOne(
     *  targetEntity="\Chill\PersonBundle\Entity\Person"
     * )
     * @Assert\NotNull()
     */
    private $person;
    
    /**
     *
     * @var Scope
     * @ORM\ManyToOne(
     *  targetEntity="\Chill\MainBundle\Entity\Scope"
     *  ) 
     * @Assert\NotNull()
     */
    private $circle;
    
    /**
     * @var boolean
     * @ORM\Column(name="closed", type="boolean", options={ "default"=false })
     */
    private $closed = false;
    
    public function __construct()
    {
        
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return AbstractTask
     */
    public function setType($type)
    {
        $this->type = (string) $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set currentStates
     * 
     * The current states are sorted in a single array, non associative.
     *
     * @param $currentStates
     *
     * @return AbstractTask
     */
    public function setCurrentStates($currentStates)
    {
        $this->currentStates = \array_keys($currentStates);

        return $this;
    }

    /**
     * Get currentStates
     * 
     * The states are returned as required by marking store format. 
     *
     * @return array
     */
    public function getCurrentStates()
    {
        return \array_fill_keys($this->currentStates, 1);
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return AbstractTask
     */
    public function setTitle($title)
    {
        $this->title = (string) $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return AbstractTask
     */
    public function setDescription($description)
    {
        $this->description = (string) $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    public function getAssignee(): ?User
    {
        return $this->assignee;
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    public function getCircle(): ?Scope
    {
        return $this->circle;
    }
    
    public function setAssignee(User $assignee = null)
    {
        $this->assignee = $assignee;
        return $this;
    }

    public function setPerson(Person $person)
    {
        $this->person = $person;
        return $this;
    }

    public function setCircle(Scope $circle)
    {
        $this->circle = $circle;
        return $this;
    }

    public function getCenter(): ?\Chill\MainBundle\Entity\Center
    {
        if ($this->getPerson() instanceof Person) {
            return $this->getPerson()->getCenter();
        }
        
        return null;
        
    }
    
    public function getScope(): ?\Chill\MainBundle\Entity\Scope
    {
        return $this->getCircle();
    }

    /**
     * @return bool
     */
    public function isClosed(): bool
    {
        return $this->closed;
    }
    
    /**
     * 
     * @param bool $closed
     */
    public function setClosed(bool $closed)
    {
        $this->closed = $closed;
    }
}

