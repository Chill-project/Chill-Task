<?php

namespace Chill\TaskBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * RecurringTask
 *
 * @ORM\Table(name="chill_task.recurring_task")
 * @ORM\Entity(repositoryClass="Chill\TaskBundle\Repository\RecurringTaskRepository")
 */
class RecurringTask extends AbstractTask
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="first_occurence_end_date", type="date")
     */
    private $firstOccurenceEndDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_occurence_end_date", type="date")
     */
    private $lastOccurenceEndDate;

    /**
     * @var string
     *
     * @ORM\Column(name="occurence_frequency", type="string", length=255)
     */
    private $occurenceFrequency;

    /**
     * @var dateinterval
     *
     * @ORM\Column(name="occurence_start_date", type="dateinterval")
     */
    private $occurenceStartDate;

    /**
     * @var dateinterval
     *
     * @ORM\Column(name="occurence_warning_interval", type="dateinterval", nullable=true)
     */
    private $occurenceWarningInterval;
    
    /**
     *
     * @var Collection
     * 
     * @ORM\OneToMany(
     *  targetEntity="SingleTask",
     *  mappedBy="recurringTask"
     * )
     */
    private $singleTasks;


    public function __construct()
    {
        $this->singleTasks = new ArrayCollection();
    }

    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstOccurenceEndDate
     *
     * @param \DateTime $firstOccurenceEndDate
     *
     * @return RecurringTask
     */
    public function setFirstOccurenceEndDate($firstOccurenceEndDate)
    {
        $this->firstOccurenceEndDate = $firstOccurenceEndDate;

        return $this;
    }

    /**
     * Get firstOccurenceEndDate
     *
     * @return \DateTime
     */
    public function getFirstOccurenceEndDate()
    {
        return $this->firstOccurenceEndDate;
    }

    /**
     * Set lastOccurenceEndDate
     *
     * @param \DateTime $lastOccurenceEndDate
     *
     * @return RecurringTask
     */
    public function setLastOccurenceEndDate($lastOccurenceEndDate)
    {
        $this->lastOccurenceEndDate = $lastOccurenceEndDate;

        return $this;
    }

    /**
     * Get lastOccurenceEndDate
     *
     * @return \DateTime
     */
    public function getLastOccurenceEndDate()
    {
        return $this->lastOccurenceEndDate;
    }

    /**
     * Set occurenceFrequency
     *
     * @param string $occurenceFrequency
     *
     * @return RecurringTask
     */
    public function setOccurenceFrequency($occurenceFrequency)
    {
        $this->occurenceFrequency = $occurenceFrequency;

        return $this;
    }

    /**
     * Get occurenceFrequency
     *
     * @return string
     */
    public function getOccurenceFrequency()
    {
        return $this->occurenceFrequency;
    }

    /**
     * Set occurenceStartDate
     *
     * @param dateinterval $occurenceStartDate
     *
     * @return RecurringTask
     */
    public function setOccurenceStartDate($occurenceStartDate)
    {
        $this->occurenceStartDate = $occurenceStartDate;

        return $this;
    }

    /**
     * Get occurenceStartDate
     *
     * @return dateinterval
     */
    public function getOccurenceStartDate()
    {
        return $this->occurenceStartDate;
    }

    /**
     * Set occurenceWarningInterval
     *
     * @param dateinterval $occurenceWarningInterval
     *
     * @return RecurringTask
     */
    public function setOccurenceWarningInterval($occurenceWarningInterval)
    {
        $this->occurenceWarningInterval = $occurenceWarningInterval;

        return $this;
    }

    /**
     * Get occurenceWarningInterval
     *
     * @return dateinterval
     */
    public function getOccurenceWarningInterval()
    {
        return $this->occurenceWarningInterval;
    }
}

