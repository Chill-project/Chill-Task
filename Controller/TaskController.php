<?php

namespace Chill\TaskBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Chill\TaskBundle\Repository\SingleTaskRepository;
use Chill\TaskBundle\Security\Authorization\TaskVoter;
use Symfony\Component\Workflow\Registry;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Chill\TaskBundle\Event\UI\UIEvent;
use Chill\TaskBundle\Entity\AbstractTask;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Workflow\Transition;


class TaskController extends Controller
{
    /**
     * Apply a transition to a task
     *
     * @Route(
     *  "/{_locale}/task/transition/{kind}/{taskId}/{transition}",
     *  name="chill_task_task_transition"
     * )
     *
     * @param string $kind
     * @param int $taskId
     * @param string $transition
     * @param SingleTaskRepository $singleTaskRepository
     * @param Registry $registry
     * @param EntityManagerInterface $em
     * @param Request $request
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function applyTransitionAction(
        $kind,
        $taskId,
        $transition,
        SingleTaskRepository $singleTaskRepository,
        Registry $registry,
        EntityManagerInterface $em,
        Request $request,
        TranslatorInterface $translator,
        EventDispatcherInterface $eventDispatcher
    ) {
        switch ($kind) {
            case 'single-task':
                $task = $singleTaskRepository
                    ->find($taskId)
                    ;
                $defaultReturnPath = $this->generateUrl(
                    'chill_task_single_task_show',
                    [ 
                        'id' => $task->getId(),
                        'list_params' => $request->query->get('list_params', [])
                    ]);
                $defaultTemplate = '@ChillTask/SingleTask/transition.html.twig';
                break;
            default:
                return new Response("The type '$kind' is not implemented",
                    Response::HTTP_BAD_REQUEST);
        }

        if (NULL === $task) {
            throw $this->createNotFoundException("task with id '$taskId' and type "
                . "'$type' does not exists");
        }
        
        $workflow = $registry->get($task);
        
        if (!$workflow->can($task, $transition)) {
            throw $this->createAccessDeniedException('You are not allowed to apply this transition');
        }
        $transitionInstance = \array_values( // array_values needed to reset keys (array_filter preserves keys)
            \array_filter(
                        $workflow->getEnabledTransitions($task), 
                        function(Transition $t) use ($transition) {
                            return $t->getName() === $transition;
                        }
                     ))[0];
        
        $form = $this->createTransitionForm($task);
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {

            if ($workflow->can($task, $transition)) {
                $workflow->apply($task, $transition);

                $em->flush();

                $this->addFlash('success', $translator->trans('The transition is successfully applied'));

            } else {
                $this->addFlash('error', $translator->trans('The transition could not be applied'));
            }
            
            return $this->redirect($defaultReturnPath);
        } else {
            $event = (new UIEvent($kind, $task))
                ->setForm($form)
                ->setTransition($transitionInstance)
                ;
            
            $eventDispatcher->dispatch(UIEvent::SHOW_TRANSITION_PAGE, $event);
            
            if ($event->hasResponse()) {
                return $event->getResponse();
            } else {
                // we simply check that the user can see the task. Other ACL checks
                // should be performed using `guard` events.
                $this->denyAccessUnlessGranted(TaskVoter::SHOW, $task);
                
                return $this->render($defaultTemplate, [
                    'task' => $task,
                    'form' => $form->createView(),
                    'transition' => $transitionInstance
                ]);
            }
        }
    }
    
    /**
     * 
     * @param \Chill\TaskBundle\Controller\AbstractTask $task
     * @return \Symfony\Component\Form\FormInterface
     */
    protected function createTransitionForm(AbstractTask $task)
    {
        $builder = $this->createFormBuilder($task);
        $builder->add('submit', SubmitType::class);
        
        return $builder->getForm();
    }
}
