<?php

namespace Chill\TaskBundle\Controller;

use Chill\PersonBundle\Privacy\PrivacyEvent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Doctrine\ORM\EntityManager;
use Chill\PersonBundle\Entity\Person;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Chill\TaskBundle\Entity\SingleTask;
use Chill\TaskBundle\Form\SingleTaskType;
use Chill\TaskBundle\Form\SingleTaskListType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormFactoryInterface;
use Chill\TaskBundle\Security\Authorization\TaskVoter;
use Symfony\Component\Security\Core\Role\Role;
use Chill\MainBundle\Pagination\PaginatorFactory;
use Chill\TaskBundle\Repository\SingleTaskRepository;
use Chill\MainBundle\Entity\User;
use Chill\PersonBundle\Security\Authorization\PersonVoter;
use Chill\PersonBundle\Repository\PersonRepository;
use Chill\MainBundle\Entity\UserRepository;
use Chill\TaskBundle\Event\TaskEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Chill\TaskBundle\Event\UI\UIEvent;
use Chill\MainBundle\Repository\CenterRepository;


class SingleTaskController extends Controller
{
    
    /**
     * @var EventDispatcherInterface
     */
    protected $eventDispatcher;
    
    /**
     * SingleTaskController constructor.
     *
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }
    
    
    /**
     * @Route(
     * "/{_locale}/task/single-task/new",
     * name="chill_task_single_task_new"
     * )
     */
    public function newAction(
        Request $request,
        TranslatorInterface $translator
    ) {

      $task = (new SingleTask())
          ->setAssignee($this->getUser())
          ->setType('task_default')
          ;

        if ($request->query->has('person_id')) {
            $personId = $request->query->getInt('person_id', null);

            if ($personId === null) {
                return new Response("You must provide a person_id", Response::HTTP_BAD_REQUEST);
            }

            $person = $this->getDoctrine()->getManager()
                ->getRepository(Person::class)
                ->find($personId);

            if ($person === null) {
                $this->createNotFoundException("Invalid person id");
            }

            $task->setPerson($person);
        }

        $this->denyAccessUnlessGranted(TaskVoter::CREATE, $task, 'You are not '
            . 'allowed to create this task');

        $form = $this->setCreateForm($task, new Role(TaskVoter::CREATE));

        $form->handleRequest($request);
        
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($task);

                $this->eventDispatcher->dispatch(TaskEvent::PERSIST, new TaskEvent($task));

                $em->flush();

                $this->addFlash('success', $translator->trans("The task is created"));

                return $this->redirectToRoute('chill_task_singletask_list', [
                    'person_id' => $task->getPerson()->getId()
                ]);

            } else {
                $this->addFlash('error', $translator->trans("This form contains errors"));
            }
        }

        return $this->render('ChillTaskBundle:SingleTask:new.html.twig', array(
            'form' => $form->createView(),
            'task' => $task
        ));
    }


    /**
     * @Route(
     * "/{_locale}/task/single-task/{id}/show",
     * name="chill_task_single_task_show"
     * )
     */
    public function showAction(Request $request, $id)
    {
        /* @var $taskRepository SingleTaskRepository */
        $taskRepository = $this->get('chill_task.single_task_repository');

        $task = $taskRepository->find($id);

        if (!is_null($task->getPerson() === !null)) {
            $personId = $task->getPerson()->getId();

            if ($personId === null) {
                return new Response("You must provide a person_id", Response::HTTP_BAD_REQUEST);
            }

            $person = $this->getDoctrine()->getManager()
                ->getRepository(Person::class)
                ->find($personId);

            if ($person === null) {
                throw $this->createNotFoundException("Invalid person id");
            }
        }
        $this->denyAccessUnlessGranted(TaskVoter::SHOW, $task, 'You are not '
            . 'allowed to view this task');

        if (!$task) {
            throw $this->createNotFoundException('Unable to find Task entity.');
        }

        $timeline = $this->get('chill.main.timeline_builder')
            ->getTimelineHTML('task', array('task' => $task));
    
        $event = new PrivacyEvent($person, array(
            'element_class' => SingleTask::class,
            'element_id' => $task->getId(),
            'action' => 'show'
        ));
        $this->eventDispatcher->dispatch(PrivacyEvent::PERSON_PRIVACY_EVENT, $event);
        
        return $this->render('ChillTaskBundle:SingleTask:show.html.twig', array(
            'task'      => $task,
            'timeline'  => $timeline
        ));
    }


    /**
     * @Route(
     * "/{_locale}/task/single-task/{id}/edit",
     * name="chill_task_single_task_edit"
     * )
     */
    public function editAction(
        Request $request,
        $id,
        TranslatorInterface $translator
    ) {
        /* @var $taskRepository SingleTaskRepository */
        $taskRepository = $this->get('chill_task.single_task_repository');

        $task = $taskRepository->find($id);

        if (!is_null($task->getPerson() === !null)) {
            $personId = $task->getPerson()->getId();
            if ($personId === null) {
                return new Response("You must provide a person_id", Response::HTTP_BAD_REQUEST);
            }

            $person = $this->getDoctrine()->getManager()
                ->getRepository(Person::class)
                ->find($personId);

            if ($person === null) {
                throw $this->createNotFoundException("Invalid person id");
            }
        }
        $this->denyAccessUnlessGranted(TaskVoter::UPDATE, $task, 'You are not '
            . 'allowed to edit this task');

        if (!$task) {
            throw $this->createNotFoundException('Unable to find Task entity.');
        }
        
        $event = (new UIEvent('single-task', $task))
            ->setForm($this->setCreateForm($task, new Role(TaskVoter::UPDATE)))
            ;
        
        $this->eventDispatcher->dispatch(UIEvent::EDIT_FORM, $event);
        
        $form = $event->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($task);

                $em->flush();

                $this->addFlash('success', $translator
                    ->trans("The task has been updated"));
    
                $event = new PrivacyEvent($person, array(
                    'element_class' => SingleTask::class,
                    'element_id' => $task->getId(),
                    'action' => 'update'
                ));
                $this->eventDispatcher->dispatch(PrivacyEvent::PERSON_PRIVACY_EVENT, $event);

                return $this->redirectToRoute(
                    'chill_task_singletask_list', 
                    $request->query->get('list_params', [])
                );

            } else {
                $this->addFlash('error', $translator->trans("This form contains errors"));
            }
        }
        
        $this->eventDispatcher->dispatch(UIEvent::EDIT_PAGE, $event);
        
        if ($event->hasResponse()) {
            return $event->getResponse();
        }
    
        $event = new PrivacyEvent($person, array(
            'element_class' => SingleTask::class,
            'element_id' => $task->getId(),
            'action' => 'edit'
        ));
        $this->eventDispatcher->dispatch(PrivacyEvent::PERSON_PRIVACY_EVENT, $event);
    
        return $this->render('ChillTaskBundle:SingleTask:edit.html.twig', array(
            'task'      => $task,
            'form'      => $form->createView()
        ));
    }


    /**
     * @Route(
     * "/{_locale}/task/single-task/{id}/delete",
     * name="chill_task_single_task_delete"
     * )
     */
    public function deleteAction(
        Request $request,
        $id,
        TranslatorInterface $translator
    ) {
        /* @var $taskRepository SingleTaskRepository */
        $taskRepository = $this->get('chill_task.single_task_repository');

        $task = $taskRepository->find($id);

        if (!$task) {
            throw $this->createNotFoundException('Unable to find Task entity.');
        }

        if (!is_null($task->getPerson() === !null)) {

            $personId = $task->getPerson()->getId();
            if ($personId === null) {
                return new Response("You must provide a person_id", Response::HTTP_BAD_REQUEST);
            }

            $person = $this->getDoctrine()->getManager()
                ->getRepository(Person::class)
                ->find($personId);

            if ($person === null) {
                throw $this->createNotFoundException("Invalid person id");
            }

        }

        $this->denyAccessUnlessGranted(TaskVoter::DELETE, $task, 'You are not '
            . 'allowed to delete this task');

        $form = $this->createDeleteForm($id);

        if ($request->getMethod() === Request::METHOD_DELETE) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $logger = $this->get('chill.main.logger');

                $logger->notice("A task has been removed", array(
                   'by_user' => $this->getUser()->getUsername(),
                   'task_id' => $task->getId(),
                   'description' => $task->getDescription(),
                   'assignee' => $task->getAssignee(),
                   'scope_id' => $task->getScope()->getId(),
                   //'start_date' => $task->getStartDate()->format('Y-m-d'),
                   //'end_date' => $task->getEndDate()->format('Y-m-d'),
                   //'warning_interval' => $task->getWarningInterval()->format('Y-m-d')
                ));

                $em = $this->getDoctrine()->getManager();
                $em->remove($task);
                $em->flush();

                $this->addFlash('success', $translator
                      ->trans("The task has been successfully removed."));

                return $this->redirect($this->generateUrl(
                  'chill_task_singletask_list', 
                  $request->query->get('list_params', [
                      'person_id' => $person->getId()
                  ])));
            }
        }


        return $this->render('ChillTaskBundle:SingleTask:confirm_delete.html.twig', array(
           'task' => $task,
           'delete_form' => $form->createView()
        ));
    }

    /**
     *
     * @param SingleTask $task
     * @param Role $role
     * @return \Symfony\Component\Form\FormInterface
     */
    protected function setCreateForm(SingleTask $task, Role $role)
    {
        $form = $this->createForm(SingleTaskType::class, $task, [
            'center' => $task->getCenter(),
            'role'   => $role
        ]);

        $form->add('submit', SubmitType::class);

        return $form;
    }

    /**
     *
     * @return Response
     * @Route(
     *   "/{_locale}/task/single-task/list/my",
     *   name="chill_task_single_my_tasks"
     * )
     */
    public function myTasksAction(TranslatorInterface $translator)
    {
        return $this->redirectToRoute('chill_task_singletask_list', [
            'user_id' => $this->getUser()->getId(),
            'hide_form' => true,
            'title' => $translator->trans('My tasks')
        ]);
    }

    /**
     *
     * Arguments:
     *  - user_id
     *  - scope_id
     *  - person_id
     *  - hide_form (hide the form to filter the tasks)
     *  - status: date state, amongst SingleTaskRepository::DATE_STATUSES, or 'closed'
     *
     * @Route(
     *  "/{_locale}/task/singletask/list",
     *  name="chill_task_singletask_list"
     * )
     */
    public function listAction(
        Request $request,
        PaginatorFactory $paginatorFactory,
        SingleTaskRepository $taskRepository,
        PersonRepository $personRepository,
        CenterRepository $centerRepository,
        FormFactoryInterface $formFactory
    ) {
        /* @var $viewParams array The parameters for the view */
        /* @var $params array The parameters for the query */

        $viewParams['person'] = null;
        $params['person'] = null;
        $viewParams['user'] = null;
        $params['user'] = null;
        $viewParams['center'] = null;
        $params['types'] = null;

        // Get parameters from url
        if (!empty($request->query->get('person_id', NULL))) {
            $personId = $request->query->getInt('person_id');
            $person = $personRepository->find($personId);

            if ($person === null) {
                 throw $this->createNotFoundException("This person ' $personId ' does not exist.");
            }

            $this->denyAccessUnlessGranted(PersonVoter::SEE, $person);

            $viewParams['person'] = $person;
            $params['person'] = $person;
        } elseif (!empty($request->query->get('center_id', NULL))) {
            $center = $centerRepository->find($request->query->getInt('center_id'));
            
            if ($center === null) {
                throw $this->createNotFoundException('center not found');
            }
            
            $params['center'] = $center;
        }
        
        if(!empty($request->query->get('types', []))) {
            $types = $request->query->get('types', []);
            if (count($types) > 0) {
                $params['types'] = $types;
            }
        }

        if (!empty($request->query->get('user_id', null))) {
            if ($request->query->get('user_id') === '_unassigned') {
                $params['unassigned'] = true;
            } else {
                $userId = $request->query->getInt('user_id', null);
                $user = $this->getDoctrine()->getManager()
                    ->getRepository('ChillMainBundle:User')
                    ->find($userId);

                if ($user === null) {
                     throw $this->createNotFoundException("This user ' $userId ' does not exist.");
                }

                $viewParams['user'] = $user;
                $params['user'] = $user;
            }
        }

        if (!empty($request->query->get('scope_id'))) {

            $scopeId = $request->query->getInt('scope_id', null);
            $scope = $this->getDoctrine()->getManager()
                ->getRepository('ChillMainBundle:Scope')
                ->find($scopeId);

            if ($scope === null) {
                 throw $this->createNotFoundException("This scope' $scopeId 'does not exist.");
            }

            $viewParams['scope'] = $scope;
            $params['scope'] = $scope;
        }

        // collect parameters for filter
        $possibleStatuses = \array_merge(SingleTaskRepository::DATE_STATUSES, [ 'closed' ]);
        $statuses = $request->query->get('status', $possibleStatuses);

        // check for invalid statuses
        $diff = \array_diff($statuses, $possibleStatuses);
        if (count($diff) > 0) {
            return new Response(
                'date_status not allowed: '. \implode(', ', $diff),
                 Response::HTTP_BAD_REQUEST
                );
        }

        $viewParams['isSingleStatus'] = $singleStatus = count($statuses) === 1;

        $tasks_count = 0;

        foreach($statuses as $status) {
            if($request->query->has('status')
                && FALSE === \in_array($status, $statuses)) {
                continue;
            }

            // different query if regarding to date or 'closed'
            if (in_array($status, SingleTaskRepository::DATE_STATUSES)) {
                $params['date_status'] = $status;
                $params['is_closed']   = false;
            } else {
                $params['date_status'] = null;
                $params['is_closed']   = true;
            }

            $count = $taskRepository
                ->countByParameters($params, $this->getUser())
                ;
            $paginator = $paginatorFactory->create($count);

            $viewParams['single_task_'.$status.'_count'] = $count;
            $viewParams['single_task_'.$status.'_paginator'] = $paginator;
            $viewParams['single_task_'.$status.'_tasks'] = $taskRepository
                ->findByParameters($params, $this->getUser(),
                    $singleStatus ? $paginator->getCurrentPage()->getFirstItemNumber() : 0,
                    $singleStatus ? $paginator->getItemsPerPage() : 10)
                ;

            $tasks_count = $tasks_count + $count;
        }

        // total number of tasks
        $viewParams['tasks_count'] = $tasks_count;

        if ($viewParams['person'] !== null){
            $viewParams['layout'] = 'ChillPersonBundle::layout.html.twig';
        } else {
            $viewParams['layout'] = 'ChillMainBundle::layout.html.twig';
        }

        // Form for filtering tasks
        $form = $formFactory->createNamed(null, SingleTaskListType::class, null, [
            'person' => $viewParams['person'],
            'method' => Request::METHOD_GET,
            'csrf_protection' => false,
            'add_type' => true
        ]);

        $form->handleRequest($request);
    
        if (isset($person)) {
            $event = new PrivacyEvent($person, array(
                'element_class' => SingleTask::class,
                'action' => 'list'
            ));
            $this->eventDispatcher->dispatch(PrivacyEvent::PERSON_PRIVACY_EVENT, $event);
        }
        
        return $this->render('ChillTaskBundle:SingleTask:index.html.twig',
            \array_merge($viewParams, [ 'form' => $form->createView() ]));
    }


    protected function getPersonParam(Request $request, EntityManagerInterface $em)
    {
        $person = $em->getRepository(Person::class)
            ->find($request->query->getInt('person_id'))
            ;

        if (NULL === $person) {
            throw $this->createNotFoundException('person not found');
        }

        $this->denyAccessUnlessGranted(PersonVoter::SEE, $person, "You are "
            . "not allowed to see this person");

        return $person;
    }

    protected function getUserParam(Request $request, EntityManagerInterface $em)
    {
        $user = $em->getRepository(User::class)
            ->find($request->query->getInt('user_id'))
            ;

        if (NULL === $user) {
            throw $this->createNotFoundException('user not found');
        }

        return $user;
    }

    /**
     * Creates a form to delete a Task entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl(
                'chill_task_single_task_delete',
                array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', SubmitType::class, array('label' => 'Delete'))
            ->getForm()
        ;
    }

}
