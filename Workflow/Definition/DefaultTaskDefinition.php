<?php
/*
 * Copyright (C) 2018 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\TaskBundle\Workflow\Definition;

use Chill\TaskBundle\Entity\AbstractTask;
use Chill\TaskBundle\Entity\SingleTask;
use Symfony\Component\Workflow\Transition;

/**
 *
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class DefaultTaskDefinition implements \Chill\TaskBundle\Workflow\TaskWorkflowDefinition
{
    const TRANSITION_METADATA = [
        'close' => [
            'verb' => 'close',
            'class' => 'sc-button bt-task-label bt-task-close',
            'sentence' => '%user% has closed the task',
            'sentence_confirmation' => 'Are you sure you want to close this task ?',
            'apply_transition_submit_label' => 'Close_verb'
        ],
        'cancel' => [
            'verb' => 'cancel',
            'class' => 'sc-button bt-task-label bt-task-cancel',
            'sentence' => '%user% has canceled the task',
            'sentence_confirmation' => 'Are you sure you want to cancel this task ?',
            'apply_transition_submit_label' => 'Set this task to cancel state'
        ],
        'start' => [
            'verb' => 'start',
            'class' => 'sc-button bt-task-label bt-task-start',
            'sentence' => '%user% has started the task',
            'sentence_confirmation' => 'Are you sure you want to start this task ?',
            'apply_transition_submit_label' => 'Start_verb'
        ]
    ];
    
    const DEFINITION_METADATA = [
        'name' => 'Default task'
    ];

    public function supports(AbstractTask $task)
    {

        return $task instanceof SingleTask
            && $task->getType() === 'task_default';
    }

    public static function getAssociatedWorkflowName()
    {
        return 'task_default';
    }

    public function getWorkflowMetadata(
        AbstractTask $task,
        string $key,
        $metadataSubject = null
    ) {
        $keys = \explode('.', $key);

        switch($keys[0]) {
            case 'transition':
                if (!$metadataSubject instanceof Transition) {
                    throw new \LogicException("You must give a transition as metadatasubject");
                }

                return $this->getTransitionMetadata(\implode('.', \array_slice($keys, 1)), $metadataSubject);
            case 'definition':
                return self::DEFINITION_METADATA[$keys[1]] ?? $key;
            default:
                return $key;
        }
    }

    protected function getTransitionMetadata($key, Transition $transition)
    {
        if (!\array_key_exists($transition->getName(), self::TRANSITION_METADATA)) {
            return $key;
        }

        if (!\array_key_exists($key, self::TRANSITION_METADATA[$transition->getName()])) {
            return $key;
        }

        return self::TRANSITION_METADATA[$transition->getName()][$key];
    }

    public function isClosed(AbstractTask $task)
    {
        return \array_key_exists('closed', $task->getCurrentStates())
            || \array_key_exists('canceled', $task->getCurrentStates());
    }
}
