<?php
/*
 * Copyright (C) 2018 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\TaskBundle\Workflow;

use Chill\TaskBundle\Entity\AbstractTask;
use Symfony\Component\Workflow\SupportStrategy\SupportStrategyInterface;
use Symfony\Component\Workflow\Workflow;
use Symfony\Component\Workflow\Event\Event;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class TaskWorkflowManager implements SupportStrategyInterface
{
    /**
     *
     * @var TaskWorkflowDefinition[]
     */
    protected $definitions = array();
    
    public function addDefinition(TaskWorkflowDefinition $definition) {
        $this->definitions[] = $definition;
    }
    
    /**
     * 
     * @param AbstractTask $task
     * @return TaskWorkflowDefinition
     * @throws \LogicException
     */
    public function getTaskWorkflowDefinition(AbstractTask $task)
    {
        $definitions = array();
        
        foreach($this->definitions as $tested) {
            if ($tested->supports($task)) {
                $definitions[] = $tested;
            }
        }
        
        $count = count($definitions);
        if ($count > 1) {
            throw new \LogicException("More than one TaskWorkflowDefinition supports "
                . "this task. This should not happens.");
        } elseif ($count === 0) {
            throw new \LogicException(\sprintf("No taskWorkflowDefinition supports this task type: %s (task id: %s).", $task->getType(), $task->getId()));
        }
        
        return $definitions[0];
    }
    
    public function supports(Workflow $workflow, $subject): bool
    {
        if (!$subject instanceof AbstractTask) {
            return false;
        }
        
        return $workflow->getName() === $this
            ->getTaskWorkflowDefinition($subject)->getAssociatedWorkflowName();
    }
    
    public function getWorkflowMetadata(AbstractTask $task, string $key, $metadataSubject = null, string $name = null)
    {
        return $this->getTaskWorkflowDefinition($task)
            ->getWorkflowMetadata($task, $key, $metadataSubject);
    }
    
    public function onTaskStateEntered(Event $e) 
    {
        $task = $e->getSubject();
        
        $definition = $this->getTaskWorkflowDefinition($task);
        
        $task->setClosed($definition->isClosed($task));
    }
}
