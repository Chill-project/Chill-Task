<?php
/*
 * 
 */
namespace Chill\TaskBundle\Event\UI;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Response;
use Chill\TaskBundle\Entity\AbstractTask;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Workflow\Transition;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class UIEvent extends Event
{
    const SHOW_TRANSITION_PAGE = 'chill_task.show_transition_page';
    const EDIT_FORM = 'chill_task.edit_form';
    const EDIT_PAGE = 'chill_task.edit_page';
    
    /**
     *
     * @var string
     */
    protected $kind;
    
    /**
     *
     * @var AbstractTask 
     */
    protected $task;
    
    /**
     *
     * @var Response|null
     */
    protected $response = null;
    
    /**
     *
     * @var FormInterface|null
     */
    protected $form = null;
    
    /**
     * @var Transition
     */
    protected $transition = null;
    
    public function __construct($kind, AbstractTask $task)
    {
        $this->kind = $kind;
        $this->task = $task;
    }
    
    /**
     * 
     * @return string
     */
    public function getKind()
    {
        return $this->kind;
    }

    /**
     * 
     * @return AbstractTask
     */
    public function getTask(): AbstractTask
    {
        return $this->task;
    }
    
    /**
     * 
     * @return FormInterface|null
     */
    public function getForm()
    {
        return $this->form;
    }

    public function setForm(FormInterface $form)
    {
        $this->form = $form;
        
        return $this;
    }
    
    /**
     * 
     * @return Transition
     */
    public function getTransition()
    {
        return $this->transition;
    }

    public function setTransition(Transition $transition)
    {
        $this->transition = $transition;
        
        return $this;
    }
        
    /**
     * 
     * @return Response
     */
    public function getResponse(): Response
    {
        return $this->response;
    }

    /**
     * 
     * @param Response $response
     * @return $this
     */
    public function setResponse(Response $response)
    {
        $this->response = $response;
        
        return $this;
    }

    public function hasResponse()
    {
        return $this->response instanceof Response;
    }


}
