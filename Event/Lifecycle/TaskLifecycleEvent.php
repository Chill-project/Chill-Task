<?php
/*
 * Copyright (C) 2018 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\TaskBundle\Event\Lifecycle;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Chill\TaskBundle\Event\TaskEvent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Chill\TaskBundle\Entity\Task\SingleTaskPlaceEvent;
use Symfony\Component\Workflow\Event\Event as WorkflowEvent;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class TaskLifecycleEvent implements EventSubscriberInterface
{
    /**
     *
     * @var TokenStorageInterface
     */
    protected $tokenStorage;
    
    /**
     *
     * @var EntityManagerInterface
     */
    protected $em;
    
    public function __construct(
        TokenStorageInterface $tokenStorage, 
        EntityManagerInterface $em
    ) {
        $this->tokenStorage = $tokenStorage;
        $this->em = $em;
    }

    
    public static function getSubscribedEvents(): array
    {
        return [
            TaskEvent::PERSIST => [
                'onTaskPersist'
            ]
        ];
    }
    
    public function onTaskPersist(TaskEvent $e)
    {
        $task = $e->getTask();
        $user = $this->tokenStorage->getToken()->getUser();
        
        $event = (new SingleTaskPlaceEvent())
            ->setTask($task)
            ->setAuthor($user)
            ->setTransition('_creation')
            ->setData([
                'new_states' => $task->getCurrentStates()
            ])
            ;
        
        $task->getTaskPlaceEvents()->add($event);
        
        $this->em->persist($event);
    }
    
    public function onTransition(WorkflowEvent $e)
    {
        $task = $e->getSubject();
        $user = $this->tokenStorage->getToken()->getUser();
        
        $event = (new SingleTaskPlaceEvent())
            ->setTask($task)
            ->setAuthor($user)
            ->setTransition($e->getTransition()->getName())
            ->setData([
                'old_states' => $e->getTransition()->getFroms(),
                'new_states' => $e->getTransition()->getTos(),
                'workflow' => $e->getWorkflowName()
            ])
            ;
        
        $task->getTaskPlaceEvents()->add($event);
        
        $this->em->persist($event);
    }
    
}
