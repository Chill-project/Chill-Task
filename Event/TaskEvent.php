<?php
/*
 * Copyright (C) 2018 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\TaskBundle\Event;

use Chill\TaskBundle\Entity\AbstractTask;
use Symfony\Component\EventDispatcher\Event;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class TaskEvent extends Event
{
    const PERSIST = 'chill_task.task_persist';
    
    /**
     *
     * @var AbstractTask
     */
    protected $task;
    
    public function __construct(AbstractTask $task)
    {
        $this->task = $task;
    }
    
    public function getTask(): AbstractTask
    {
        return $this->task;
    }

    public function setTask(AbstractTask $task)
    {
        $this->task = $task;
        
        return $this;
    }
}

