<?php
/*
 * Copyright (C) 2018 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\TaskBundle\Timeline;

use Chill\MainBundle\Timeline\TimelineProviderInterface;
use Doctrine\ORM\EntityManagerInterface;
use Chill\TaskBundle\Entity\Task\SingleTaskPlaceEvent;
use Chill\TaskBundle\Entity\SingleTask;
use Symfony\Component\Workflow\Registry;
use Symfony\Component\Workflow\Workflow;

/**
 *
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class SingleTaskTaskLifeCycleEventTimelineProvider implements TimelineProviderInterface
{
    /**
     *
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     *
     * @var Registry
     */
    protected $registry;

    const TYPE = 'chill_task.transition';

    public function __construct(EntityManagerInterface $em, Registry $registry)
    {
        $this->em = $em;
        $this->registry = $registry;
    }

    public function fetchQuery($context, $args)
    {
        if ($context !== 'task') {
            throw new \LogicException(sprintf('%s is not able '
                    . 'to render context %s', self::class, $context));
        }

        $metadata = $this->em
            ->getClassMetadata(SingleTaskPlaceEvent::class);
        $singleTaskMetadata = $this->em
            ->getClassMetadata(SingleTask::class);

        return [
            'id' => sprintf('%s.%s.%s', $metadata->getSchemaName(), $metadata->getTableName(), $metadata->getColumnName('id')),
            'type' => self::TYPE,
            'date' => $metadata->getColumnName('datetime'),
            'FROM' => sprintf('%s JOIN %s ON %s = %s',
                sprintf('%s.%s', $metadata->getSchemaName(), $metadata->getTableName()),
                sprintf('%s.%s', $singleTaskMetadata->getSchemaName(), $singleTaskMetadata->getTableName()),
                $metadata->getAssociationMapping('task')['joinColumns'][0]['name'],
                sprintf('%s.%s.%s', $singleTaskMetadata->getSchemaName(), $singleTaskMetadata->getTableName(), $singleTaskMetadata->getColumnName('id'))
                ),
            'WHERE' => sprintf('%s.%s = %d',
                sprintf('%s.%s', $singleTaskMetadata->getSchemaName(), $singleTaskMetadata->getTableName()),
                $singleTaskMetadata->getColumnName('id'),
                $args['task']->getId()
                )
        ];

    }

    public function getEntities(array $ids)
    {
        $events = $this->em
            ->getRepository(SingleTaskPlaceEvent::class)
            ->findBy([ 'id' => $ids ])
            ;

        return \array_combine(
            \array_map(function($e) { return $e->getId(); }, $events ),
            $events
            );
    }

    public function getEntityTemplate($entity, $context, array $args)
    {
        $workflow = $this->registry->get($entity->getTask(), $entity->getData['workflow']);
        $transition = $this->getTransitionByName($entity->getTransition(), $workflow);

        return [
            'template' => 'ChillTaskBundle:Timeline:single_task_transition_task_context.html.twig',
            'template_data' => [
                'task' => $args['task'],
                'event' => $entity,
                'transition' => $transition
            ]
        ];
    }

    /**
     *
     * @param string $name
     * @param Workflow $workflow
     * @return \Symfony\Component\Workflow\Transition
     */
    protected function getTransitionByName($name, Workflow $workflow)
    {
        foreach ($workflow->getDefinition()->getTransitions() as $transition) {
            if ($transition->getName() === $name) {
                return $transition;
            }
        }
    }

    public function supportsType($type): bool
    {
        return $type === self::TYPE;
    }
}
