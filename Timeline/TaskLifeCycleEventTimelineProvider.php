<?php
/*
 * Copyright (C) 2018 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\TaskBundle\Timeline;

use Chill\MainBundle\Timeline\TimelineProviderInterface;
use Doctrine\ORM\EntityManagerInterface;
use Chill\TaskBundle\Entity\Task\SingleTaskPlaceEvent;
use Chill\TaskBundle\Entity\SingleTask;
use Symfony\Component\Workflow\Registry;
use Symfony\Component\Workflow\Workflow;
use Chill\MainBundle\Security\Authorization\AuthorizationHelper;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Chill\ActivityBundle\Security\Authorization\ActivityVoter;
use Symfony\Component\Security\Core\Role\Role;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class TaskLifeCycleEventTimelineProvider implements TimelineProviderInterface
{
    /**
     *
     * @var EntityManagerInterface
     */
    protected $em;
    
    /**
     *
     * @var Registry
     */
    protected $registry;
    
    /**
     *
     * @var AuthorizationHelper
     */
    protected $authorizationHelper;
    
    /**
     *
     * @var TokenStorageInterface
     */
    protected $tokenStorage;
    
    const TYPE = 'chill_task.transition';
    
    public function __construct(
        EntityManagerInterface $em, 
        Registry $registry,
        AuthorizationHelper $authorizationHelper,
        TokenStorageInterface $tokenStorage
    ) {
        $this->em = $em;
        $this->registry = $registry;
        $this->authorizationHelper = $authorizationHelper;
        $this->tokenStorage = $tokenStorage;
    }

    public function fetchQuery($context, $args)
    {
        if ($context !== 'person') {
            throw new \LogicException(sprintf('%s is not able '
                    . 'to render context %s', self::class, $context));
        }
        
        $metadata = $this->em
            ->getClassMetadata(SingleTaskPlaceEvent::class);
        $singleTaskMetadata = $this->em
            ->getClassMetadata(SingleTask::class);
        $user = $this->tokenStorage->getToken()->getUser();
        $circles = $this->authorizationHelper->getReachableCircles(
            $user, new Role(ActivityVoter::SEE_DETAILS), $args['person']->getCenter());
        
        
        if (count($circles) > 0) {
            $circlesId = \array_map(function($c) { return $c->getId(); }, $circles);
            $circleRestriction = sprintf('%s.%s.%s IN (%s)', 
                $singleTaskMetadata->getSchemaName(), // chill_task schema
                $singleTaskMetadata->getTableName(), // single_task table name
                $singleTaskMetadata->getAssociationMapping('circle')['joinColumns'][0]['name'],
                \implode(', ', $circlesId)
                );
        } else {
            $circleRestriction = 'FALSE = TRUE';
        }
        
        
        return [
            'id' => sprintf('%s.%s.%s', $metadata->getSchemaName(), $metadata->getTableName(), $metadata->getColumnName('id')),
            'type' => self::TYPE,
            'date' => $metadata->getColumnName('datetime'),
            'FROM' => sprintf('%s JOIN %s ON %s = %s', 
                sprintf('%s.%s', $metadata->getSchemaName(), $metadata->getTableName()),
                sprintf('%s.%s', $singleTaskMetadata->getSchemaName(), $singleTaskMetadata->getTableName()),
                $metadata->getAssociationMapping('task')['joinColumns'][0]['name'],
                sprintf('%s.%s.%s', $singleTaskMetadata->getSchemaName(), $singleTaskMetadata->getTableName(), $singleTaskMetadata->getColumnName('id'))
                ),
            'WHERE' => sprintf('%s.%s = %d and %s',
                sprintf('%s.%s', $singleTaskMetadata->getSchemaName(), $singleTaskMetadata->getTableName()),
                $singleTaskMetadata->getAssociationMapping('person')['joinColumns'][0]['name'],
                $args['person']->getId(),
                $circleRestriction
                )
        ];
    } 
    
    public function getEntities(array $ids) 
    {
        $events = $this->em
            ->getRepository(SingleTaskPlaceEvent::class)
            ->findBy([ 'id' => $ids ])
            ;
        
        return \array_combine(
            \array_map(function($e) { return $e->getId(); }, $events ),
            $events
            );
    } 
    
    public function getEntityTemplate($entity, $context, array $args)
    {
        $workflow = $this->registry->get($entity->getTask(), $entity->getData['workflow']);
        $transition = $this->getTransitionByName($entity->getTransition(), $workflow);
        
        return [
            'template' => 'ChillTaskBundle:Timeline:single_task_transition_person_context.html.twig',
            'template_data' => [ 
                'person' => $args['person'],
                'event' => $entity,
                'transition' => $transition
            ]
        ];
    } 
    
    /**
     * 
     * @param string $name
     * @param Workflow $workflow
     * @return \Symfony\Component\Workflow\Transition
     */
    protected function getTransitionByName($name, Workflow $workflow)
    {
        foreach ($workflow->getDefinition()->getTransitions() as $transition) {
            if ($transition->getName() === $name) {
                return $transition;
            }
        }
    }
    
    public function supportsType($type): bool
    {
        return $type === self::TYPE;
    }
}
