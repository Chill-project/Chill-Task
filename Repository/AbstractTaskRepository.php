<?php

namespace Chill\TaskBundle\Repository;

/**
 * AbstractTaskRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
abstract class AbstractTaskRepository extends \Doctrine\ORM\EntityRepository
{
}
