<?php
/*
 * Copyright (C) 2018 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\TaskBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Chill\MainBundle\Form\Type\ChillDateType;
use Chill\MainBundle\Entity\Center;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Chill\MainBundle\Form\Type\UserPickerType;
use Chill\MainBundle\Form\Type\ScopePickerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Role\Role;
use Chill\TaskBundle\Security\Authorization\TaskVoter;
use Chill\MainBundle\Form\Type\DateIntervalType;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class SingleTaskType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class)
            ->add('description', TextareaType::class, [
                'required' => false
            ])
            ->add('assignee', UserPickerType::class, [
                'required' => false,
                'center' => $options['center'],
                'role'   => $options['role'],
                'placeholder' => 'Not assigned'
            ])
            ->add('circle', ScopePickerType::class, [
                'center' => $options['center'],
                'role' => $options['role']
            ])
            ->add('startDate', ChillDateType::class, [
                'required' => false
            ])
            ->add('endDate', ChillDateType::class, [
                'required' => false
            ])
            ->add('warningInterval', DateIntervalType::class, [
                'required' => false
            ])
            ;
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setRequired('center')
            ->setAllowedTypes('center', [ Center::class ])
            ->setRequired('role')
            ->setAllowedTypes('role', [ Role::class ])
            ;
    }
}
