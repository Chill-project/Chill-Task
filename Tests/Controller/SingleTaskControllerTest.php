<?php

namespace Chill\TaskBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Chill\MainBundle\Tests\TestHelper;
use Faker;
use Chill\MainBundle\Entity\Center;

class SingleTaskControllerTest extends WebTestCase
{
    /**
     *
     * @var Faker\Generator
     */
    protected $faker;
    
    protected function setUp()
    {
        self::bootKernel();
        $this->faker = Faker\Factory::create('fr');
    }
    
    /**
     * 
     * @return \Chill\PersonBundle\Entity\Person
     */
    protected function getRandomPerson($centerName)
    {
        $em = self::$kernel
            ->getContainer()
            ->get('doctrine.orm.entity_manager')
            ;
        
        $centers = $em
            ->getRepository(Center::class)
            ->findAll();
        
        $center = \array_filter(
            $centers, 
            function(Center $c) use ($centerName) {
                return $centerName === $c->getName();
            })[0];
        
        $ids = $em
            ->createQuery('SELECT p.id FROM ChillPersonBundle:Person p '
                . 'WHERE p.center = :center'
                )
            ->setParameter('center', $center)
            ->getResult()
            ;
        
        $id = $ids[\array_rand($ids)];
        
        return self::$kernel
            ->getContainer()
            ->get('doctrine.orm.entity_manager')
            ->getRepository(\Chill\PersonBundle\Entity\Person::class)
            ->find($id)
            ;
    }
    
    public function testNew()
    {
        $client = static::createClient(
            array(), 
            TestHelper::getAuthenticatedClientOptions()
            );
        $person = $this->getRandomPerson('Center A');

        $crawler = $client->request('GET', '/fr/task/single-task/new', [
            'person_id' => $person->getId()
        ]);
        var_dump($crawler->text());
        
        $this->assertTrue($client->getResponse()->isSuccessful());
        
        
        
        $form = $crawler->selectButton('Envoi')->form();
        
        $title = $this->faker->sentence;
        $circles = $form->get('circle')
            ->availableOptionsValues()
            ;
        
        $client->submit($form, [
            'title' => $title,
            'circle' => $circles[\array_rand($circles)]
        ]);
        
        $this->assertTrue($client->getResponse()->isRedirect(sprintf(
            '/fr/task/task/list/%d', $person->getId())));
        
        $crawler = $client->followRedirect();
        
        $this->assertContains($title, $crawler->text(),
            "Assert that newly created task title is shown in list page")
            ;
    }

}
